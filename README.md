# Computer Vision Problem

This challenge does not require any specific knowledge of CV, just some Machine Learning skills & willingness to play around.

Assumptions:
1. You have an image with *4* red dots
2. The exact RGB code of the dots is (255, 39, 39)
3. There's nothing else in the image that has the same colour of the dots.

Task:
Given an image saved as a png file on disk, you need to write a script to find an approximation of the coordinates of the centers of the 4 dots (*return the (x,y) tuples*).

To find the dots you are **required** to use a *clustering* algorithm (any).
**Note: You may use any programming language**.

Suggestions (for Python users):
1. In general this task can be achieved with OpenCV, NumPy and Scikit-Learn
2. Opening an image with OpenCV2 will return a NumPy matrix of shape (Y, X, 3). The third coordinate represents a **BGR** colour array.
3. To filter the image for the red dots you might use `cv2.inRange` function (giving the image and BGR colour bounds)
4. Any clustering algorithm of Sklearn can be used on a particular transformation of the matrix returned by inRange
5. Also note the `cluster_centers_` attribute of some implementations of clustering algorithms such as *KMeans* in sklearn.

You can find an example image in this repository, called *image.png*.

### Bonus Question

Suppose you have two images with two sets of 4 points (the images can have different sizes and the 4 points are in different positions in each image).
Ideally, we would like to match these two sets of points, in a way such that the point on the top-left corner of the first image gets matched to the top-left corner point of the second image (same for the bottom-left, top-right, bottom-right).

You might use a distance metric, such as the Euclidean distance, but note that there might be a point which is closer than any other point to two or more corners. There might be as well a corner which is closer to two or more points than any other corner. 
We need to handle these situations somehow as we want a unique one-to-one matching.

How would you solve this problem? Implement your solution.

### Submission 

Please submit a Git repository (hosted on any git provider) to giulio@quickalgorithm.com containing the following two files  : 
1. A script/executable file which given as an input the image path (e.g. through console argument) returns the coordinates of the points
2. A text/markdown file briefly explaining your solution and/or challenges encountered while trying to solve the problem

<sub><sup>PS: The bonus question is not really optional</sup></sub>

